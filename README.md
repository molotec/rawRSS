# rawRSS

rawRSS aims to be a simple and sleek open-source (and forever free) RSS Reader.

## Future Features

* Default dark theme
* Intentionally without social networks integrations (maybe as toggleable plugins in the future).
* Formats supported
    * Atom
    * RSS 2.0
    * YouTube channels (coming soon)
    * JSONFeed (not yet)

More info in the [wiki](https://gitlab.com/molotec/rawRSS/wikis/home)

## How to install

TODO
